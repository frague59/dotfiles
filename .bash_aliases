#!/bin/sh

test -x "/usr/bin/exa" && alias ls="exa"
test -x "/usr/bin/ncdu" && alias ncdu="ncdu --color dark"
test -x "/usr/bin/codium" && alias code="codium"
test -x "/usr/bin/meld" && alias meld="meld --gtk-no-debug"
alias zzz="systemctl suspend"
