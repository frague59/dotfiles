#!/usr/bin/env bash

up() { 
    if [ "${1/[^0-9]/}" == "$1" ]; then 
        p=./; for i in $(seq 1 $1); 
        do p=${p}../; done; 
        cd $p; 
    else 
        echo 'usage: up N'; fi 
}

export -f up
